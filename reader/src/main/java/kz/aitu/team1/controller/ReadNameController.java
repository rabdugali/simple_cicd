package kz.aitu.team1.controller;

import kz.aitu.team1.model.Name;
import kz.aitu.team1.repository.NameRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping(value = "/name")
public class ReadNameController {

    public final NameRepository nameRepository;
    public ReadNameController(NameRepository nameRepository) {
        this.nameRepository = nameRepository;
    }

    @GetMapping("/get-names")
    public String getNames() {
        List<Name> Names = nameRepository.findAll();
        return Names.toString();
    }
}
