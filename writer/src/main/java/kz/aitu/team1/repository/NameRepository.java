package kz.aitu.team1.repository;

import kz.aitu.team1.model.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface NameRepository extends JpaRepository<Name, Long> {
    List<Name> findByName(String name);
}