package kz.aitu.team1.controller;

import kz.aitu.team1.model.Location;
import kz.aitu.team1.model.Name;
import kz.aitu.team1.repository.LocationRepository;
import kz.aitu.team1.repository.NameRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping(value = "/name")
public class WriteNameController {

    private final NameRepository nameRepository;
    public WriteNameController(NameRepository nameRepository) { this.nameRepository = nameRepository; }

    @GetMapping("/add-name")
    public String addName(@RequestParam(value = "name", defaultValue = "World") String name) {
        if (!name.isEmpty() && !name.equals("World")) { nameRepository.save(new Name(name)); }
        return String.format("Hello %s!", name);
    }

}
