package kz.aitu.team1.controller;

import kz.aitu.team1.model.Location;
import kz.aitu.team1.repository.LocationRepository;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping(value = "/location")
public class WriteLocationController {
    private final LocationRepository locRepository;
    public WriteLocationController(LocationRepository locRepository) { this.locRepository = locRepository; }

//    @GetMapping("/add")
//    public String addLocation(@RequestParam(value = "lat") Double lat, @RequestParam(value = "lon") Double lon) {
//        locRepository.save(new Location(lat, lon));
//        return lat.toString() + " " + lon.toString();
//    }

    @PostMapping("/add")
    public String addLocation(@RequestBody Location location) {
        locRepository.save(location);
        return location.getLat().toString() + " " + location.getLon().toString();
    }
}
